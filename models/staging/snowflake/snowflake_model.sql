with ecomm as (
  select * from {{ source('DEMO_DB', 'INTEGRATION_TESTING_OWAIS') }}
),

final as (
  select * from ecomm
)
select * from final
